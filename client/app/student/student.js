/**
 * Created by nvcanh on 14-Mar-17.
 */
(function(){
    angular.module('student.module',[]).controller("StudentController",function($scope, $http, socket){
        var url = '/api/student/';
        var getAllStudent = function () {
            $http({
                method : 'GET',
                params: {},
                url :url + 'allStudent'
            }).then(function(response) {
                $scope.students = response.data;
            }, function(response) {
            });
        };

        $scope.deleteStudent = function (studentId) {
            $http({
                method : 'DELETE',
                params: {},
                url :url +studentId
            }).then(function(response) {
                $scope.students = response.data;
            }, function(response) {
            });
        };
        //$scope.testSocket = function (data) {
        //    socket.emit('chat message', data);
        //};
        //socket.on('chat message', function(msg){
        //    $scope.status =msg;
        //});


        getAllStudent();

    })
})();