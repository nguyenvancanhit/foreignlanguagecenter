/**
 * Created by nvcanh on 16-Mar-17.
 */
(function(){
    angular.module('chat.module',[]).controller("ChatController",function($scope, $http, socket){
        $scope.messages = [];

        $scope.sentMessage = function (data) {
            if(data != ''){
                socket.emit('chat message', data);
            }
            $scope.data = '';
        };
        socket.on('chat message', function(msg){
            $scope.messages.push(msg);
        });
        $scope.clearData = function(){
            $scope.messages = [];
        }

    })
})();