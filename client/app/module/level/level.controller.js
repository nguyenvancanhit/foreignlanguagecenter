/**
 * Created by nvcanh on 17-Mar-17.
 */
(function(){
    angular.module('level.module',[]).controller("LevelController",function($scope, $http, socket){
        var url = '/api/level/';
        $scope.levels = [];
        var getAllLevel = function () {
            $http({
                method : 'GET',
                params: {},
                url :url + 'allLevel'
            }).then(function(response) {
                $scope.levels = response.data;
                socket.syncUpdates('level',$scope.levels, function (a,b,c ) {
                    console.log(a);
                    console.log(b);
                    console.log(c);
                });
            }, function(response) {
            });
        };

        $scope.deleteLevel = function (levelId) {
            $http({
                method : 'DELETE',
                params: {},
                url :url +levelId
            }).then(function(response) {
                $scope.status = response.data;
                socket.syncUpdates('level',$scope.levels, function (a,b,c ) {
                    console.log('kaka');

                });
            }, function(response) {
            });

        };
        $scope.createLevel = function () {
            $http({
                method : 'POST',
                params: {},
                url :url,
                data:{name: $scope.name}
            }).then(function(response) {
                $scope.statusPost = response.data;
            }, function(response) {
            });
        }

        //$scope.testSocket = function (data) {
        //    socket.emit('chat message', data);
        //};
        //socket.on('chat message', function(msg){
        //    $scope.status =msg;
        //});


        getAllLevel();

    })
})();