/**
 * Created by nvcanh on 14-Mar-17.
 */
(function () {
    'use strict';
    angular.module('app.third-party', [
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngSanitize',
        'ngMaterial',
        'nvd3',
        'pascalprecht.translate',
        'ui.router',
        'validation.match',
        'ngMdIcons',
        'md.data.table',
        'btford.socket-io'
    ]);

    angular.module('app.service', ['socket.service']);
    angular.module('app.directive', ['keyPress.directive']);
    angular.module('app.module', [
        'home.module',
        'example.module',
        'student.module',
        'chat.module',
        'level.module'
    ]);

    angular.module('myApp', [
        'app.third-party',
        'app.service',
        'app.directive',
        'app.module'
    ]);

    var myController = function ($scope) {

    }
    angular.module('myApp').controller('myController', myController);

})();

