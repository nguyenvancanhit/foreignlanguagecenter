/**
 * Created by nvcanh on 16-Mar-17.
 */
(function () {
    'use strict';
    angular.module("keyPress.directive",[]).directive('myEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.myEnter);
                    });
                    event.preventDefault();
                }
            });
        };
    });
})();
