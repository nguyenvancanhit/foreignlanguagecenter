/**
 * Created by nvcanh on 14-Mar-17.
 */
(function ()
{
    'use strict';

    angular
        .module('myApp')
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider, $locationProvider)
    {
        //$locationProvider.html5Mode(true);
        //$locationProvider.html5Mode(true);


        $urlRouterProvider.otherwise('/home');

        // Inject $cookies
        var $cookies;

        angular.injector(['ngCookies']).invoke([
            '$cookies', function (_$cookies)
            {
                $cookies = _$cookies;
            }
        ]);
        // State definitions
        $stateProvider

            .state('home',{
                url: '/home',
                templateUrl: 'app/home/home.html',
                controller: 'HomeController'
            })
            .state('example',{
                url: '/example',
                templateUrl: 'app/example/example.html',
                controller: 'ExampleController'
            })
            .state('student',{
                url: '/student',
                templateUrl: 'app/student/student.html',
                controller: 'StudentController'
            })
            .state('chat',{
                url: '/chat',
                templateUrl: 'app/module/chat/chat.html',
                controller: 'ChatController'
            })
            .state('level',{
                url: '/level',
                templateUrl: 'app/module/level/level.html',
                controller: 'LevelController'
            })
    }

})();
