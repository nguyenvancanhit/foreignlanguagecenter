/**
 * Created by VanCanh on 3/13/2017.
 */
'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var StudentSchema = new Schema({
    name:		{type: String, required: true}
});

module.exports = mongoose.model('students', StudentSchema);