/**
 * Created by VanCanh on 3/13/2017.
 */
'use strict';

var express = require('express'),
    router = express.Router(),
    Student = require('./student.model'),
    controller = {};

controller.getStudentById = function (req, res) {
    var studentId = req.params.studentId;
    var query = Student.findById(studentId);
    query.exec(function (err, studentData) {
        if (err) {
            res.status(500).send(err);
        } else {
            if (!church) {
                return res.status(404).end();
            }
            res.status(200).json(studentData);
        }
    })
};

controller.getAllStudent = function (req, res) {
    console.log('test');
    var query = Student.find();
    query.exec(function (err, allStudentData) {
        if (err) {
            res.status(400).send(err);
        } else {
            res.status(200).json(allStudentData);
        }
    })
};

controller.deleteStudent = function (req, res) {
    console.log(req.param);
    var studentId = req.params.studentId;
    var query = Student.findByIdAndRemove(studentId);
    query.exec(function (err) {
        if (err) {
            res.status(500).send(err);
        } else {
            var query2 = Student.find();
            query2.exec(function (err, allStudentData) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    res.status(200).json(allStudentData);
                }
            })
        }
    })
};

module.exports = controller;
