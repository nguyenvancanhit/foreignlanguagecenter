/**
 * Created by VanCanh on 3/13/2017.
 */
'use strict';
var express = require('express'),
    controller = require('./student.controller'),
    router = express.Router();
    router.get('/student/:studentId', controller.getStudentById);
    router.get('/allStudent', controller.getAllStudent);
    router.delete('/:studentId', controller.deleteStudent);

module.exports = router;