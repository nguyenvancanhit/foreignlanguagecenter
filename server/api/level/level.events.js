/**
 * Gopyquangcao model events
 */

'use strict';

var  EventEmitter = require('events');
var  Level =  require('./level.model');
var LevelEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
LevelEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove',
  'findByIdAndRemove':'findByIdAndRemove'
};

// Register the event emitter to the model events
for (var e in events) {
  let event = events[e];
    Level.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  console.log('canh chagne');
  return function(doc) {
    LevelEvents.emit(event + ':' + doc._id, doc);
    LevelEvents.emit(event, doc);
  }
}


module.exports = LevelEvents;
