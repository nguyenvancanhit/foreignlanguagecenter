/**
 * Created by nvcanh on 17-Mar-17.
 */
var express = require('express'),
    router = express.Router(),
    Level = require('./level.model'),
    controller = {};

controller.getAllLevel = function(req, res){
    console.log('get all level');
    var query = Level.find();
    query.exec(function (err, allLevelData) {
        if (err) {
            res.status(400).send(err);
        } else {
            res.status(200).json(allLevelData);
        }
    })
};

controller.createLevel = function (req, res) {
    var newLevel = new Level(req.body);
    newLevel.save(function (err, levelData) {
        if(err) {
            // res.status(500).send(err);
            return res.send();
        } else {
            res.status(200).json(levelData);
        }
    })
}

// controller.deleteLevel = function (req, res) {
//     console.log(req.param);
//     var levelId = req.params.levelId;
//     var query = Level.findByIdAndRemove(levelId);
//     query.exec(function (err) {
//         if (err) {
//             res.status(500).send(err);
//         } else {
//             res.status(200).json('delete success');
//         }
//     })
// };

function respondWithResult(res, statusCode) {
    statusCode = statusCode || 200;
    return function(entity) {
        if (entity) {
            res.status(statusCode).json(entity);
        }
    };
}



function removeEntity(res) {
    return function(entity) {
        if (entity) {
            return entity.remove()
                    .then(function(){
                    res.status(204).json('delete is success');
        });
        }
    };
}

function handleEntityNotFound(res) {
    return function(entity) {
        if (!entity) {
            res.status(404).end();
            return null;
        }
        return entity;
    };
}

function handleError(res, statusCode) {
    statusCode = statusCode || 500;
    return function(err) {
        res.status(statusCode).send(err);
    };
}

controller.deleteLevel = function(req, res) {
    return Level.findById(req.params.levelId).exec()
        .then(handleEntityNotFound(res))
        .then(removeEntity(res))
        .catch(handleError(res));
}

controller.vote = function(req) {
    socket.on('connection', function(socket){
        socket.on('chat message', function(msg){
            io.emit('chat message', msg);
        });
    });
}
module.exports = controller;

