/**
 * Created by nvcanh on 17-Mar-17.
 */
'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var LevelSchema = new Schema({
    name:		{type: String, required: true}
});

//LevelSchema.post('remove', function(){
//    var update = this._update;
//    if (update.$set && (update.$set.lat || update.$set.long)){
//        try{
//            var updatedObjectId = this._conditions._id; //will serve as a room
//
//            //now emit the event using nodejs event emitter
//
//           console.log('canh');
//        }catch (e){
//
//        }
//    }
//});
module.exports = mongoose.model('levels', LevelSchema);