/**
 * Created by VanCanh on 3/18/2017.
 */
/**
 * Broadcast updates to client when the model changes
 */


/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var  LevelEvents = require('./level.events');

// Model events to emit
var events = ['save', 'remove', 'findByIdAndRemove'];

var configRegister ={};
configRegister.register =  function(socket) {
    // Bind model events to socket events
    for (var i = 0, eventsLength = events.length; i < eventsLength; i++) {
        var event = events[i];
        var listener = createListener('level:' + event, socket);

        LevelEvents.on(event, listener);
        socket.on('disconnect', removeListener(event, listener));
    }
}


function createListener(event, socket) {
    return function(doc) {
        socket.emit(event, doc);
    };
}

function removeListener(event, listener) {
    return function() {
        LevelEvents.removeListener(event, listener);
    };
}
module.exports = configRegister;