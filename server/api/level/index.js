/**
 * Created by nvcanh on 17-Mar-17.
 */
'use strict';
var express = require('express'),
    controller = require('./level.controller'),
    router = express.Router();

router.get('/allLevel', controller.getAllLevel);
router.delete('/:levelId', controller.deleteLevel);
router.get('/level', controller.vote);
router.post('/', controller.createLevel);

module.exports = router;