/**
 * Created by nvcanh on 13-Mar-17.
 */

var mongoose = require('mongoose');
module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header('Content-Type', 'application/json');
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
        res.header('Access-Control-Allow-Headers', 'Authorization, Origin, X-Requested-With, Content-Type, Accept');
        next();
    });
    app.use('/api/level', require('./api/level'));
    app.use('/api/student', require('./api/student'));

}