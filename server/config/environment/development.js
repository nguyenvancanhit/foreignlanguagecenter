'use strict';

// Development specific configuration
// ==================================
module.exports = {

// MongoDB connection options
mongo: {
  uri: 'mongodb://localhost/ottracker-dev'
},

developeMode: true,

mail :  {
  host: '192.168.95.218',
  port: 25,
  auth: {
    user: 'chungho@webclub.tma.com.vn',
    pass: '123456789'
  },
  secure:false,
  tls: {
    rejectUnauthorized: false
  },
  debug:true
},

log: {
  PATH_TO_DEBUG_LOG: './logs/debug.log',
  PATH_TO_INFO_LOG: "./logs/info.log",
  PATH_TO_WARN_LOG: './logs/warn.log',
  PATH_TO_ERROR_LOG: "./logs/error.log",
  levels: {
    error: 0,
    debug: 1,
    warn: 2,
    data: 3,
    info: 4,
    verbose: 5,
    silly: 6
  },
  colors: {
    error: 'red',
    debug: 'blue',
    warn: 'yellow',
    data: 'grey',
    info: 'green',
    verbose: 'cyan',
    silly: 'magenta'
  }
},
// Seed database on startup
seedDB: true

};
