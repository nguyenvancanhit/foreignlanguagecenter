'use strict';

exports = module.exports = {
  // List of user roles
  userRoles: ['user', 'leader', 'project manager', 'senior manager', 'director', 'senior director', 'chairman']
};
