var express = require('express');
var app = express();
var server = require('http').createServer(app);
var config  = require('./config/environment');
var io = require('socket.io')(server);
var port = process.env.PORT || 9000;
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var db = require('./config/mongoDB');
var bodyParser = require('body-parser');

mongoose.connect(db.url);


require('./config/socketio').default(io);
app.use(bodyParser.json());
app.use(bodyParser.json({type:'application/vnd.api+json'}));
app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static(__dirname + '/../client'));
var routes =require('./routes');
routes(app);
// io.on('connection', function(socket){
//     socket.on('chat message', function(msg){
//         io.emit('chat message', msg);
//     });
// });


server.listen(port);
//require('./config/socket-io')(app, server.listen(port));

console.log("server start : "+port);
exports = module.exports=app;
